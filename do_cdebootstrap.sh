#!/bin/sh

set -e
set -u
set -x

. ./setup.sh

if test "$#" -ge 1; then
	if test -z "${TMPDIR:-}" && grep -q '^tmpfs /tmp tmpfs \([^ ]*,\)\?nodev\(,[^ ]*\)\? ' /proc/self/mounts; then
		# cdebootstrap hates nodev. Make a new one without nodev.
		mount tmpfs -t tmpfs /tmp -o size=256M
	fi

	export PATH="/sbin:/usr/sbin:$PATH"
	WORKDIR=$(mktemp -d)

	cleanup() {
		if test -d "$WORKDIR"; then
			rm -Rf "$WORKDIR"
		fi
	}

	trap cleanup EXIT

	rm -Rf "$WORKDIR"
	# This fails installing cdebootstrap-helper-apt, which runs apt update and breaks on unsigned repositories. Consider it a success.
	cdebootstrap --verbose "--flavour=$1" --allow-unauthenticated usrmerged-unstable "$WORKDIR" "file://$REPREPRO_BASE_DIR" || :
	./collect_aliased.sh "$WORKDIR"
	cat "$WORKDIR/aliased" 1>&3
	rm -Rf "$WORKDIR"
else
	for flavour in minimal standard; do
		container=lxc unshare -U -m --map-auto --setuid 0 --setgid 0 --pid --fork "$0" "$flavour" 3> "res/cdebootstrap-$flavour"
	done
fi
