#!/bin/sh

set -e
set -u

. ./setup.sh

mkdir -p res
for variant in debootstrap minbase apt; do
	mmdebstrap \
		"--variant=$variant" \
		--include=apt \
		"" \
		/dev/null \
		"deb [trusted=yes] copy://$REPREPRO_BASE_DIR usrmerged-unstable main" \
		--customize-hook=./collect_aliased.sh \
		--customize-hook="download aliased res/mmdebstrap-$variant"
done
