#!/bin/sh
# SPDX-License-Identifier: MIT
# /bin/sh is either bash or dash:
# shellcheck disable=SC2039

set -u
set -e

. ./setup.sh

PATCHDIR=$(realpath patches)
WORKDIR=$(mktemp -d)
MAIN_ARCH=$(dpkg --print-architecture)
OTHER_ARCHS=i386

cleanup() {
	if test -d "$WORKDIR"; then
		rm -Rf "$WORKDIR"
	fi
}

trap cleanup EXIT

if ! test -d "$REPREPRO_BASE_DIR"; then
	mkdir -p "$REPREPRO_BASE_DIR/conf"
	cat > "$REPREPRO_BASE_DIR/conf/distributions" <<EOF
Codename: usrmerged-unstable
Label: usrmerged-unstable
Origin: usrmerged-debian
Architectures: $MAIN_ARCH $OTHER_ARCHS
Components: main
UDebComponents: main
Description: updated packages for /usr-merge
EOF
	cat > "$REPREPRO_BASE_DIR/conf/options" <<EOF
verbose
ignore wrongdistribution
EOF
	reprepro export
fi

. ./setup_chdist.sh

todo=
for p in patches/*; do
	p=${p#patches/}

	if [ ! -e "patches/$p" ]; then
		echo "patches/$p doesn't exist"
		continue
	fi

	if [ ! -x "patches/$p" ]; then
		echo "patches/$p is not executable"
		continue
	fi

	# shellcheck disable=SC2016
	our_version=$(reprepro --list-format '${$SourceVersion}\n' -T deb listfilter usrmerged-unstable "\$Source (== $p)" | uniq)
	their_version=$(chdist_base apt-get source --only-source -t unstable --no-act "$p" | sed "s/^Selected version '\\([^']*\\)' (unstable) for .*/\\1/;t;d")
	if test -z "$their_version"; then
		echo "cannot determine source version for $p"
		exit 1
	fi
	if test -n "$our_version" && dpkg --compare-versions "$our_version" gt "$their_version"; then
		if [ -e repo/dists/usrmerged-unstable/Release ] && [ repo/dists/usrmerged-unstable/Release -ot "patches/$p" ]; then
			echo "patches/$p has been changed -- rebuilding"
		else
			echo "package $p up to date"
			continue
		fi
	fi

	todo="$todo $p"
done

# Since this script can also be run locally outside of a CI, we add a number
# of options to overwrite possible settings in the local ~/.sbuildrc.
#
# The --apt-upgrade and --apt-distupgrade options are necessary to make sure
# that patched packages from the essential and build-essential set are
# installed in their most recent version in the build chroot
COMMON_SBUILD_OPTS="-d unstable --nolog --no-clean-source --no-source-only-changes --no-run-lintian --no-run-autopkgtest --apt-upgrade --apt-distupgrade"

for p in $todo; do
	reprepro removesrc usrmerged-unstable "$p"

	rm -Rf "$WORKDIR"
	mkdir "$WORKDIR"
	(
		cd "$WORKDIR"
		chdist_base apt-get source --only-source -t unstable "$p"
		cd "$p-"*
		"$PATCHDIR/$p"
		dch --local "+usrmerge" "apply /usr-merge patch"
		dch --release ""
		version=$(dpkg-parsechangelog --show-field Version | sed 's/^[0-9]\+://')
		if chdist_base apt-cache showsrc -t unstable "$p" | grep -v arch=all | grep -q arch=; then
			for a in $OTHER_ARCHS; do
				DEB_BUILD_OPTIONS="nocheck noautodbgsym" sbuild "--arch=$a" --no-arch-all --arch-any --profiles=nocheck $COMMON_SBUILD_OPTS
				env -C .. reprepro include usrmerged-unstable "./${p}_${version}_${a}.changes"
			done
		fi
		DEB_BUILD_OPTIONS="nocheck noautodbgsym" sbuild --arch-all --arch-any --profiles=nocheck \
			$COMMON_SBUILD_OPTS
		cd ..

		reprepro include usrmerged-unstable "./${p}_${version}_${MAIN_ARCH}.changes"
	)
	rm -Rf "$WORKDIR"
done

for p in $(reprepro --list-format '${$source}\n' -T deb listfilter usrmerged-unstable '$Version (% *usrmerge*)' | sed 's/^\([^ (]\+\).*/\1/' | sort -u); do
	if [ ! -x "patches/$p" ]; then
		echo "patches/$p is not exist as executable --removing from repo"
		reprepro removesrc usrmerged-unstable "$p"
		continue
	fi
done
