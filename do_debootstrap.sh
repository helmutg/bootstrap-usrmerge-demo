#!/bin/sh

set -e
set -u
set -x

. ./setup.sh

if test "$#" -ge 1; then
	# auto-apt-proxy fails inside a user namespace. Disable inside.
	if test "$(apt-config shell V Acquire::http::Proxy-Auto-Detect)" = "V='/usr/bin/auto-apt-proxy'"; then
		mount --bind /bin/true /usr/bin/auto-apt-proxy
	fi

	export PATH="/sbin:/usr/sbin:$PATH"
	WORKDIR=$(mktemp -d)

	cleanup() {
		if test -d "$WORKDIR"; then
			rm -Rf "$WORKDIR"
		fi
	}

	trap cleanup EXIT

	rm -Rf "$WORKDIR"
	debootstrap --verbose $(test "$1" = debootstrap || echo --variant "$1") --no-check-gpg usrmerged-unstable "$WORKDIR" "file://$REPREPRO_BASE_DIR" unstable
	./collect_aliased.sh "$WORKDIR/"
	cat "$WORKDIR/aliased" 1>&3
	rm -Rf "$WORKDIR"
else
	# auto-apt-proxy fails inside a user namespace. Handle it outside.
	if test "$(apt-config shell V Acquire::http::Proxy-Auto-Detect)" = "V='/usr/bin/auto-apt-proxy'"; then
		http_proxy=$(auto-apt-proxy)
		if test -n "$http_proxy"; then
			export http_proxy
		else
			unset http_proxy
		fi
	fi

	for variant in debootstrap minbase; do
		container=lxc unshare -U -m --map-auto --setuid 0 --setgid 0 "$0" "$variant" 3> "res/debootstrap-$variant"
	done
fi
