#!/bin/sh

set -u
set -e

. ./setup.sh
. ./setup_chdist.sh

WORKDIR=$(mktemp -d)
cleanup() {
	if test -d "$WORKDIR"; then
		rm -Rf "$WORKDIR"
	fi
}

for pkg in $(/usr/sbin/debootstrap --print-debs --include usrmerge unstable "$WORKDIR/debootstrap" "$MIRROR"); do
	repover=$(reprepro --list-format '${Version}\n' -T deb list usrmerged-unstable "$pkg" | uniq)
	test "${repover%+usrmerge*}" = "$repover" || continue
	pkgver=$(chdist_base apt-cache show "$pkg" | grep-dctrl -n -sVersion .)
	test "$pkgver" = "$repover" && continue
	cd "$WORKDIR"
	chdist_base apt-get download -t unstable "$pkg"
	reprepro remove usrmerged-unstable "$pkg"
	reprepro includedeb usrmerged-unstable "./${pkg}_"*.deb
	rm "./${pkg}_"*.deb
	cd ..
done
reprepro export
