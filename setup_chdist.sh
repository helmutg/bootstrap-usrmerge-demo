. ./setup.sh

chdistdata=$(pwd)/chdist
chdist_base() {
	local cmd
	cmd=$1
	shift
	chdist "--data-dir=$chdistdata" "$cmd" base "$@"
}
if [ ! -d "$chdistdata" ]; then
	chdist_base create
fi
cat << END > "$chdistdata/base/etc/apt/sources.list"
deb $MIRROR unstable main
deb-src $MIRROR unstable main
END
cat <<END > "$chdistdata/base/etc/apt/apt.conf.d/customize.conf"
Acquire::Languages "none";
END
chdist_base apt-get update
