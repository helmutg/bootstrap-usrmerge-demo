#!/bin/sh

(
	cd "$1/var/lib/dpkg/info"
	grep '^/\(s\?bin\|lib\)' *.list 
) | sed 's,^\([^:]*\)\(:[0-9a-z]*\)\?\.list:,\1 ,' > "$1/aliased"
