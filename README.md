How to use this?
================

Install the following packages on bookworm or later:

    cdebootstrap dctrl-tools debootstrap devscripts mmdebstrap mount reprepro sbuild uidmap

Build (or rebuild) packages with patches. They'll end up below `repo`.

    ./update.sh

Import packages without patches. They'll end up below `repo`.

    ./importdebs.sh

Run bootstrapping tools. The results show up in `res`.

    ./do_cdebootstrap.sh
    ./do_debootstrap.sh
    ./do_mmdebstrap.sh

Ideally all files in `res` contain only top level entries from base-files.
